PIP:="venv/bin/pip"
SETTINGS={{ project_name }}.settings
TEST_SETTINGS={{ project_name }}.test_settings

define create-env
python -m venv env
endef

all: clean venv static migrate runserver
venv:
	@$(create-venv)

freeze: 
	@$(PIP) install django whitenoise dj-database-url gunicorn psycopg2-binary coverage selenium
	@$(PIP) > freeze > requirements.txt
	@$(PIP) install -r requirements.txt

static:
	python manage.py collectstatic --no-input

runserver:
	python manage.py runserver 8000 &

migrate:
	python manage.py makemigrations
	python manage.py migrate

clean:
	django-admin.py clean_pyc --settings=$(SETTINGS)



